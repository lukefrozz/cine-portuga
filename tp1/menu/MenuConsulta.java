package tp1.menu;

import tp1.cinema.Cinema;
import tp1.cinema.Sala;
import tp1.cinema.Sessao;
import tp1.cinema.testaSala;

import java.util.Vector;

import consola.SConsola;
import est.tempo.Data;

/**
 * classe respons�vel pelo menu de consultas
 * @author autor1, autor2
 *
 */
public class MenuConsulta {
	private SConsola consola = new SConsola();
	private Cinema cinema;
	
	public MenuConsulta( Cinema cinema ){
		this.cinema = cinema;
	}

	/**
	 * apresenta o menu de consultas
	 */
	public void menuConsultas(){
		String menu = "Consulta\n\n1- Ver filmes" + 
		              "\n2- Procurar filme" +
		              "\n3- Mostrar sala" +
		              "\n0 - sair  \n\nopção> ";
		
		char op = ' ';
		do {
			consola.clear();
			consola.print( menu );
			op = consola.readChar();
			switch( op ){
			case '1': verFilmes(); break;
			case '2': procurarFilme(); break;
			case '3': mostrarSala(); break;
			}
		} while( op != '0' );
		consola.close();
	}

	/**
	 * apresenta os filmes
	 */
	private void verFilmes( ) {
		consola.clear();
		Vector<Sala> salas = new Vector<Sala>();
		Vector<Sessao> sessoes = new Vector<Sessao>();
		consola.println("Filmes");
		for( int i=0; i < salas.size(); i++  ){
			consola.println("Sala: " + salas.get(i) );
			// apresentar todas as sess�es
			for( int s =0; s < sessoes.size(); s++ ){
				// para cada sess�o apresentar:
				//   in�cio da sess�o, nome do filme, e lugares vagos 
				
				System.out.println(sessoes);
			}
		}
		consola.readLine();
	}
	
	/**
	 * procurar um dado filme, pelo t�tulo, ou parte dele
	 */
	private void procurarFilme() {
		consola.println("Filme a procurar: ");
		String filme = consola.readLine();
		
		// encontrar as sess�es com filmes com este t�tulo e apresent�-las
		// para cada sess�o apresentar:
		//   sala, in�cio da sess�o, nome completo do filme, e lugares vagos
		consola.readLine();
	}

	private void mostrarSala(){
		Sala sala = null;
		do {
			  consola.println("Sala: ");
			  String numSala = consola.readLine();
			  sala = cinema.getSala( numSala );
		} while( sala == null );
		
		// para cada sess�o apresentar:
		//   in�cio da sess�o, nome do filme, e lugares vagos
		consola.readLine();
	}
	
}
