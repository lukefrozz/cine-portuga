package tp1.menu;

import tp1.cinema.Cinema;
import tp1.cinema.Sala;
import tp1.cinema.Sessao;
import consola.SConsola;
import est.tempo.Data;

/**
 * Esta classe representa o menu de venda de bilhetes
 * @author autor1, autor2
 */
public class MenuBilheteira {
	private SConsola consola = new SConsola();
	private Cinema cinema;
	
	public MenuBilheteira( Cinema cinema ){
		this.cinema = cinema;
	}

	/**
	 * Apresenta o menu de venda de bilhetes
	 */
	public void menuBilhetes(){
		String nomeSala;
		do {
			consola.clear();
			consola.println("(Sem nome, para terminar)");
			consola.print("Nome sala: ");
			nomeSala = consola.readLine();
			if( !nomeSala.isEmpty() )
				venderBilhetes( nomeSala );
		} while( !nomeSala.isEmpty() );
		consola.close();
	}

	/**
	 * vende bilhetes para uma determinada sala
	 * @param nomeSala nome da sala para onde vender bilhetes
	 */
	private void venderBilhetes( String nomeSala ) {
		// procurar qual a sala com o nome indicado
		Sala sala = cinema.getSala( nomeSala); 
		if( sala == null ) {
			consola.println("Essa sala n�o existe!!!");
			consola.readLine();
			return;
		}
		consola.clear();
		consola.println("Sala " + "NOMESALA" );
		
		// listar todas as sess�es da sala
		// para cada sess�o apresentar o �ndica, vagas e nome do filme

		// depois pedir qual a sess�o para vender bilhetes
		consola.println("Para que sessao?");
		int numSess = consola.readInt() - 1;
		if( numSess < 0 || numSess >= 0 /* n�mero de sess�es poss�veis */ ){
			consola.println("sess�o inv�lida");
			consola.readLine();
			return;
		}
		
		// pedir a quantidade de bilhetes
		consola.println("Quantos bilhetes?  (max: " + "VAGAS" + ")");
		int numBilh = consola.readInt();
		// verificar se a quantidade � v�lida
		if( numBilh <= 0 || numBilh > 0 /* N�mero de vagas */ ){
			consola.println("N�mero de bilhetes inv�lido");
			consola.readLine();
			return;
		}
		
		// reservar os bilhetes
	}
}
