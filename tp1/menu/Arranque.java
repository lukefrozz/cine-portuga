package tp1.menu;

import java.util.Vector;

import est.tempo.Hora;
import tp1.cinema.Cinema;
import tp1.cinema.Sala;
import tp1.cinema.Sessao;

/**
 * Esta classe � respons�vel pelo arranque do sistema. Cria o cinema e os menus de interac��o
 * @author autor1, autor2
 */
public class Arranque {

	/**
	 * m�todo que cria o cinema e as salas
	 * @return o cinema criado
	 */
	static private Cinema setupCinema(){
		Cinema cine = new Cinema();
		
		Vector<Sessao> sessoes1 = new Vector<Sessao>();
		sessoes1.add(new Sessao(120, 0, "Piratas das caribas: homens mortos não contam histórias"));
		Vector<Sessao> sessoes2 = new Vector<Sessao>();
		Vector<Sessao> sessoesVIP = new Vector<Sessao>();
		cine.addSala(new Sala("1", new Vector<Sessao>()));
		cine.addSala(new Sala("2", new Vector<Sessao>()));
		cine.addSala(new Sala("VIP", new Vector<Sessao>()));
		
		// criar algumas salas
		/* devem criar uma cinema com 3 salas
		 * 1: com 120 lugares
		 * 2: com 100 lugares
		 * VIP: com 100 lugares
		 */
		
		// adicionar algumas sess�es
		/* devem criar as sess�es
		 *
		 * sala 1, 13:30, Piratas das caribas: homens mortos n�o contam hist�rias
		 * sala 1, 16:10, Piratas das caribas: homens mortos n�o contam hist�rias
		 * sala 1, 18:50, Piratas das caribas: homens mortos n�o contam hist�rias
		 * sala 1, 21:30, Piratas das caribas: homens mortos n�o contam hist�rias

		 * sala 2, 11:00, Amarelinho
		 * sala 2, 14:10, Amarelinho
		 * sala 2, 16:40, Amarelinho
		 * sala 2, 19:00, Alien: Covenant
		 * sala 2, 21:35, Alien: Covenant
		 * 
		 * sala VIP, 16:30, Perdidos
		 * sala VIP, 21:40, Perdidos
		 * sala VIP, 11:30, Bailarina
		 */
		
		return cine;
	}
	
	/**
	 * cria e arranca com o sistema e os menus,
	 * NÃO ALTERAR ESTE MÉTODO
	 * NÃO ALTERAR ESTE MÉTODO
	 * NÃO ALTERAR ESTE MÉTODO
	 * NÃO ALTERAR ESTE MÉTODO
	 */
	public static void main(String[] args) {
		Cinema cine = setupCinema( );

		final MenuConsulta menuCons = new MenuConsulta( cine );
		//  criar uma nova thread para ter v�rias janelas ao mesmo tempo
		Thread t1 = new Thread(){			
			public void run() {
				menuCons.menuConsultas();
			}
		};
		t1.start();
		final MenuBilheteira menuBilhetes = new MenuBilheteira( cine );
		//  criar uma nova thread para ter v�rias janelas ao mesmo tempo
		Thread t2 = new Thread(){			
			public void run() {
				menuBilhetes.menuBilhetes();
			}
		};
		t2.start();
	}

}
