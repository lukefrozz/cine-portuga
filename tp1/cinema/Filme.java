package tp1.cinema;

public class Filme {
	
	private String nomeFilme;
	private String elenco;
	
	
	public Filme(String nomeFilme, String elenco) {
		this.nomeFilme = nomeFilme;
		this.elenco = elenco;
	}
	
	public String getNomeFilme() {
		return nomeFilme;
	}
	public void setNomeFilme(String nomeFilme) {
		this.nomeFilme = nomeFilme;
	}
	public String getElenco() {
		return elenco;
	}
	public void setElenco(String elenco) {
		this.elenco = elenco;
	}
	
	
	
}
