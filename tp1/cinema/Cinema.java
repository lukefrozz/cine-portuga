package tp1.cinema;

import java.util.Vector;

/**
* Esta classe representa um complexo de cinema, composto por várias
* salas de cinema
* @author Bruno Moreira, Kimberly Lima
*/
public class Cinema {

	private Vector<Sala> salas = new Vector<Sala>();
	private Vector<Sessao> sessoes = new Vector<Sessao>();
		
	/**
	 * devolve a sala com o número dado
	 * @param numSala número da sala a aceder
	 * @return a sala correspondente ao número
	 */
	public Sala getSala( String nomeSala ){
		for (Sala sala : this.salas) {
			if(sala.getNomeSala() == nomeSala){
				return sala;
			}
		}
		return new Sala();
	}
	
	/**
	 * Adicionar uma nova sala ao complexo de cinema
	 * @param s a sala a adicionar
	 */
	public void addSala(Sala s){
		salas.add(s);
		
	}
	
	/**
	 * Indica o n�mero de salas do complexo
	 * @return o n�mero de salas do complexo
	 */
	public int getnumSalas(){
		return this.salas.size();
	}
	
	/**
	 * Retorna um array com todas as salas presentes no complexo,
	 * sem estarem ordenadas por n�meros
	 * @return array com todas as salas do cinema
	 */
	public Vector<Sala> getSalas(){
		return null;
	}
	
	
	/**
	 * Devolve todas as sess�es deste complexo cujo filme tenha a string filme no seu t�tulo
	 * @param filme O t�tulo, ou parte dele, que deve possuir o filme cujas sess�es se pretendem
	 * @return  todas as sess�es deste complexo cujo filme tenha a string filme no seu t�tulo
	 */
	public Vector<Sessao> getSessoesFilme(String filme ){		
		return null;
	}
}
