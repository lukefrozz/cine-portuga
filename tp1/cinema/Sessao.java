package tp1.cinema;

import est.tempo.Hora;
import est.tempo.Tempo;

/**
 * Esta classe representa uma sessão de cinema. Uma sessão é caracterizada por decorrer numa sala,
 * tem um filme associado e a duração da sessão, bem como o número de bilhetes vendidos para essa
 * sessão 
 * 
 * @author autor1, autor2
 */
public class Sessao {
	
	private int vagas;
	private int vendidos;
	private String filme;
	//private Tempo DiaSessao;
	//private Tempo HoraSessao;

	/*
	 * AL�M DOS CONSTRUTORES E DOS GETTERS/SETTERS DEVEM IMPLEMENTAR ESTES M�TODOS
	 * AL�M DOS CONSTRUTORES E DOS GETTERS/SETTERS DEVEM IMPLEMENTAR ESTES M�TODOS
	 * AL�M DOS CONSTRUTORES E DOS GETTERS/SETTERS DEVEM IMPLEMENTAR ESTES M�TODOS
	 */

	public Sessao(int vagas, int vendidos, String filme){
		this.vagas = vagas;
		this.vendidos = vendidos;
		//setHoraSessao(HoraSessao);
		//setDiaSessao(DiaSessao);
		this.vendidos = vendidos;
	}

	@Override
	public String toString() {
		return "Sessao [Vagas=" + vagas + ", Vendidos=" + vendidos + ", Filme=" + filme + "]";
	}


	/**
	 * devolve o n�mero de bilhetes por vender para esta sess�o
	 * @return o n�mero de bilhetes por vender para esta sess�o
	 */

	public int getVagas() {
		return vagas;
	}

	public void setVagas(int num){
		this.vagas=num;
	}
	public int getVendidos() {
		return vendidos;
	}

	public void setVendidos(int num){
		this.vendidos=getVendidos()+num;
	}
	//public Tempo getHoraSessao(){
		//return HoraSessao;
	//}
	/*
	public void setHoraSessao(Tempo dia){
		this.HoraSessao=dia;
	}
	public Tempo getDiaSessao(){
		return DiaSessao;
	}
	public void setDiaSessao(Tempo dia){
		this.DiaSessao=dia;
	}*/
	public String getFilme(){
		return filme;
	}
	public void setFilme(String nomeFilme){
		this.filme = nomeFilme;	
	}



	/**
	 * Vende bilhetes para esta sess�o
	 * @param numBilh n�mero de bilhetes a reservar para esta sess�o
	 */
	public void reservarBilhetes(int numBilhetes ) {
		if((getVagas()-getVendidos())>numBilhetes){
			System.out.printf("bilhetes rersevados com sucesso");
			setVendidos(getVendidos() + numBilhetes);
		}
		else
			System.out.printf("nao temos mais a quantidade de bilhetes disponivel");
		
	}
}
