package tp1.cinema;

import java.util.Vector;

import est.tempo.Data;
import est.tempo.Hora;
import est.tempo.Tempo;

/**
 * Esta classe representa uma sala no complexo do cinema.
 * Cada sala ser� identificada por um nome, deve ter uma capacidade m�xima e um conjunto de sess�es
 * @author autor1, autor2
 */
public class Sala {

	private String nomeSala;
	private Vector<Sessao> sessoes;
	

	public Sala() {
		sessoes = new Vector<Sessao>();
	}
	
	public Sala(String nomeSala, Vector<Sessao> sessoes) {
		this.nomeSala = nomeSala;
		this.sessoes = sessoes;
	}
	
	
	/*
	 * AL�M DOS CONSTRUTORES E DOS GETTERS/SETTERS DEVEM IMPLEMENTAR ESTES M�TODOS
	 * AL�M DOS CONSTRUTORES E DOS GETTERS/SETTERS DEVEM IMPLEMENTAR ESTES M�TODOS
	 * AL�M DOS CONSTRUTORES E DOS GETTERS/SETTERS DEVEM IMPLEMENTAR ESTES M�TODOS
	 */
	
	@Override
	public String toString() {
		return "Sala [ nomeSala=" + nomeSala + "]";
	}

	public String getNomeSala() {
		return nomeSala;
	}

	public void setNomeSala(String nomeSala) {
		this.nomeSala = nomeSala;
	}

	public Vector<Sessao> getSessoes() {
		return sessoes;
	}

	public void setSessao(Vector<Sessao> sessoes) {
		this.sessoes = sessoes;
	}

	/**
	 * Adiciona uma sess�o � sala
	 * @param s a sess�o a adicionar
	 */
	public void addSessao( Sessao s ) {
		sessoes.add(s);
	}
	
	/**
	 * Devolve as sess�es da sala num dado dia
	 * @param d dia a ver
	 * @return as sess�es nesta sala no dia d
	 */
	public Sessao getSessoes(Tempo d){
		for(int i=0; i <= sessoes.size();i++){
			Sessao c = sessoes.get(i);
			//if (d == sessao.get(i).getDiaSessao()){
			return c;
		}
		
		//}
		return null;
	
		
	}
		
}
